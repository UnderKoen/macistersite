function initImgScroller(scroller) {
    scroller.changeTab = function (i) {
        scroller.activeTab.classList.remove("active");
        scroller.activeImg.classList.remove("active");

        let img = scroller.images[i];
        img.classList.add("active");
        scroller.activeImg = img;

        let tab = scroller.tabs[i];
        tab.classList.add("active");
        scroller.activeTab = tab;
    };

    scroller.next = function () {
        let l = scroller.images.length;
        let current = scroller.images.indexOf(scroller.activeImg);
        scroller.changeTab((current + 1) % l);
    };

    scroller.images = [].slice.call(scroller.children);
    scroller.tabs = [];

    let tabs = document.createElement("span");
    tabs.classList.add("tabs");
    scroller.appendChild(tabs);

    let amount = scroller.images.length;
    for (let i = 0; i < amount; i++) {
        let img = scroller.children[i];

        let tab = document.createElement("span");
        tab.classList.add("tab");
        tab.onclick = function () {
            scroller.changeTab(i);
        };
        tabs.appendChild(tab);
        scroller.tabs[i] = tab;

        if (i === 0) {
            scroller.activeImg = img;
            scroller.activeTab = tab;

            tab.classList.add("active");
            img.classList.add("active");
        }
    }

    scroller.loop = true;

    function loopNext() {
        if (scroller.loop) scroller.next();
        setTimeout(loopNext, 1000 * 10);
    }

    setTimeout(loopNext, 1000 * 10);
}

document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll(".img-scroller").forEach(initImgScroller);
}, false);