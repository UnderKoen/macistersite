<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

$response = file_get_contents("https://accounts.magister.net/account/login?returnUrl=localhost");
preg_match("/\/js\/account-.{16}\.js/", $response, $matches);

$scriptFile = $matches[0];
$script = file_get_contents("https://accounts.magister.net$scriptFile");

$vars = [];

preg_match("/\[([A-Z](?:,[A-Z])*)\],\[(\"\d+?\"(?:,\"\d+?\")*)\]/", $script, $matches);
$varNames = $matches[1];
$varNames = explode(",", $varNames);

foreach ($varNames as $item) {
    array_push($vars, getVariable($item, $script));
}

$array = $matches[2];
$array = explode(",", $array);
$result = "";

foreach ($array as $item) {
    preg_match("/\"(.*)\"/", $item, $matches);
    $j = $matches[1];
    $j = intval($j);
    $result .= $vars[$j];
}

echo "{\"authCode\":\"$result\"}";

function getVariable($var, $script) {
    preg_match("/$var=\"((?:[a-z]|[0-9]){0,16}?)\"/", $script, $matches);
    return $matches[1];
}